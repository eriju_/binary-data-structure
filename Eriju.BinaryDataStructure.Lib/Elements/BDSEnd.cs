﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSEnd : BDSBase {
		public override byte GetType() {
			return BDSType.END;
		}

		protected override void ReadContents(BinaryReader binaryReader) {

		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			
		}

		public override string ToString() {
			return "End";
		}
	}
}
