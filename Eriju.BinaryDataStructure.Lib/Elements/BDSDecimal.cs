﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSDecimal : BDSBase {
		public decimal Value;

		public BDSDecimal() {

		}

		public BDSDecimal(string key, decimal value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.DECIMAL;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadDecimal();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
