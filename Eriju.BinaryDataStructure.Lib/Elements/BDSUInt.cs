﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSUInt : BDSBase {
		public uint Value;

		public BDSUInt() {

		}

		public BDSUInt(string key, uint value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.UINT;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadUInt32();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
