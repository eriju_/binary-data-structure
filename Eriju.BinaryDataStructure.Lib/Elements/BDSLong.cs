﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSLong : BDSBase {
		public long Value;

		public BDSLong() {

		}

		public BDSLong(string key, long value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.LONG;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadInt64();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
