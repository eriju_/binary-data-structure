﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSBool : BDSBase {
		public bool Value;

		public BDSBool() {

		}

		public BDSBool(string key, bool value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.BOOL;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadBoolean();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
