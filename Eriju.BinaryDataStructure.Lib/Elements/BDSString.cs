﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSString : BDSBase {
		public string Value;

		public BDSString() {

		}

		public BDSString(string key, string value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.STRING;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadString();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value;
		}
	}
}
