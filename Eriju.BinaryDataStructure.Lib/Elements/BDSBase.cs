﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public abstract class BDSBase {
		public string Key {
			get {
				return key ?? "";
			}

			set {
				key = value;
			}
		}

		private string key;

		public BDSBase() {
			key = null;
		}

		public BDSBase(string key) {
			this.key = key;
		}

		protected abstract void WriteContents(BinaryWriter binaryWriter);

		protected abstract void ReadContents(BinaryReader binaryReader);

		public abstract new byte GetType();

		public static BDSBase Read(BinaryReader binaryReader) {
			byte bdsType = binaryReader.ReadByte();

			if (bdsType == BDSType.END) {
				return new BDSEnd();
			}

			BDSBase bds = CreateOfType(bdsType);
			bds.Key = binaryReader.ReadString();
			bds.ReadContents(binaryReader);

			return bds;
		}

		public static void Write(BDSBase bds, BinaryWriter binaryWriter) {
			binaryWriter.Write(bds.GetType());

			if (bds.GetType() != BDSType.END) {
				binaryWriter.Write(bds.Key);
				bds.WriteContents(binaryWriter);
			}
		}

		public static BDSBase CreateOfType(byte type) {
			return (BDSBase)Activator.CreateInstance(BDSType.GetType(type));
		}
	}
}
