﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSByte : BDSBase {
		public byte Value;

		public BDSByte() {

		}

		public BDSByte(string key, byte value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.BYTE;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadByte();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
