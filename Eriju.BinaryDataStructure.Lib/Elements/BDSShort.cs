﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSShort : BDSBase {
		public short Value;

		public BDSShort() {

		}

		public BDSShort(string key, short value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.SHORT;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadInt16();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
