﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSSByte : BDSBase {
		public sbyte Value;

		public BDSSByte() {

		}

		public BDSSByte(string key, sbyte value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.SBYTE;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadSByte();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
