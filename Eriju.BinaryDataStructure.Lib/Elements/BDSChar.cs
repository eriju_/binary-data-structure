﻿using Eriju.BinaryDataStructure.Lib.Structure;
using System.IO;

namespace Eriju.BinaryDataStructure.Lib.Elements {
	public class BDSChar : BDSBase {
		public char Value;

		public BDSChar() {

		}

		public BDSChar(string key, char value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.CHAR;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadChar();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
