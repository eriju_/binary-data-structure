﻿using Eriju.BinaryDataStructure.Lib.Elements;
using System;
using System.IO;
using System.IO.Compression;

namespace Eriju.BinaryDataStructure.Lib {
	public static class BDS {
		public const short IDENTIFIER = 30500;
		public const int VERSION = 1;

		public static BDSCompound Read(BinaryReader binaryReader, out int version) {
			short identifier = binaryReader.ReadInt16();

			if (identifier != IDENTIFIER) {
				throw new NotSupportedException("This format is not supported!");
			}

			version = binaryReader.ReadInt32();

			if (version > VERSION) {
				throw new NotSupportedException($"Expected version is {VERSION} or older, but found {version}!");
			}

			return (BDSCompound)BDSBase.Read(binaryReader);
		}

		public static BDSCompound Read(BinaryReader binaryReader) {
			return Read(binaryReader, out _);
		}

		public static void Write(BDSCompound bds, BinaryWriter binaryWriter) {
			binaryWriter.Write(IDENTIFIER);
			binaryWriter.Write(VERSION);
			BDSBase.Write(bds, binaryWriter);
		}
	}
}
