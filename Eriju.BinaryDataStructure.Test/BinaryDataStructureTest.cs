﻿using System;
using System.Diagnostics;
using System.IO;
using Eriju.BinaryDataStructure.Lib;
using Eriju.BinaryDataStructure.Lib.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Eriju.BinaryDataStructure.Test {
	[TestClass]
	public class BinaryDataStructureTest {
		[TestMethod]
		public void TestMethod1() {
			BDSCompound root = new BDSCompound("Root");

			BDSInt int1 = new BDSInt("Integer", 123);
			BDSFloat float1 = new BDSFloat("Float", 123.456f);
			BDSString string1 = new BDSString("String", "Hello World!");
			BDSCompound compound1 = new BDSCompound("Compound");
			BDSDecimal decimal1 = new BDSDecimal("Decimal", 321.123456789m);

			compound1.Add(decimal1);
			root.Add(int1, float1, string1, compound1);

			using (BinaryWriter w = new BinaryWriter(File.Open("test.bin", FileMode.Create))) {
				BDS.Write(root, w);
			}

			BDSCompound bds;

			using (BinaryReader r = new BinaryReader(File.Open("test.bin", FileMode.Open))) {
				bds = BDS.Read(r);
			}

			Debug.WriteLine(root.ToString());
			Debug.WriteLine(bds.ToString());

			Assert.AreEqual(root.ToString(), bds.ToString());
		}
	}
}
