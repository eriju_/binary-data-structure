﻿namespace Eriju.BinaryDataStructure.Viewer {
	partial class FormViewer {
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.mnsMenu = new System.Windows.Forms.MenuStrip();
			this.tsmFile = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmFileOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.sepFile = new System.Windows.Forms.ToolStripSeparator();
			this.tsmFileExit = new System.Windows.Forms.ToolStripMenuItem();
			this.tsmAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.scnView = new System.Windows.Forms.SplitContainer();
			this.trvStructureView = new System.Windows.Forms.TreeView();
			this.tlpDataView = new System.Windows.Forms.TableLayoutPanel();
			this.lblKey = new System.Windows.Forms.Label();
			this.lblKeyValue = new System.Windows.Forms.Label();
			this.lblValue = new System.Windows.Forms.Label();
			this.lblValueValue = new System.Windows.Forms.Label();
			this.lblType = new System.Windows.Forms.Label();
			this.lblTypeValue = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.lblVersionValue = new System.Windows.Forms.Label();
			this.mnsMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.scnView)).BeginInit();
			this.scnView.Panel1.SuspendLayout();
			this.scnView.Panel2.SuspendLayout();
			this.scnView.SuspendLayout();
			this.tlpDataView.SuspendLayout();
			this.SuspendLayout();
			// 
			// mnsMenu
			// 
			this.mnsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFile,
            this.tsmAbout});
			this.mnsMenu.Location = new System.Drawing.Point(0, 0);
			this.mnsMenu.Name = "mnsMenu";
			this.mnsMenu.Size = new System.Drawing.Size(800, 24);
			this.mnsMenu.TabIndex = 0;
			this.mnsMenu.Text = "Menu";
			// 
			// tsmFile
			// 
			this.tsmFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFileOpen,
            this.sepFile,
            this.tsmFileExit});
			this.tsmFile.Name = "tsmFile";
			this.tsmFile.Size = new System.Drawing.Size(37, 20);
			this.tsmFile.Text = "File";
			// 
			// tsmFileOpen
			// 
			this.tsmFileOpen.Name = "tsmFileOpen";
			this.tsmFileOpen.Size = new System.Drawing.Size(103, 22);
			this.tsmFileOpen.Text = "Open";
			// 
			// sepFile
			// 
			this.sepFile.Name = "sepFile";
			this.sepFile.Size = new System.Drawing.Size(100, 6);
			// 
			// tsmFileExit
			// 
			this.tsmFileExit.Name = "tsmFileExit";
			this.tsmFileExit.Size = new System.Drawing.Size(103, 22);
			this.tsmFileExit.Text = "Exit";
			// 
			// tsmAbout
			// 
			this.tsmAbout.Name = "tsmAbout";
			this.tsmAbout.Size = new System.Drawing.Size(52, 20);
			this.tsmAbout.Text = "About";
			// 
			// scnView
			// 
			this.scnView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scnView.Location = new System.Drawing.Point(0, 24);
			this.scnView.Name = "scnView";
			// 
			// scnView.Panel1
			// 
			this.scnView.Panel1.Controls.Add(this.trvStructureView);
			// 
			// scnView.Panel2
			// 
			this.scnView.Panel2.Controls.Add(this.tlpDataView);
			this.scnView.Size = new System.Drawing.Size(800, 426);
			this.scnView.SplitterDistance = 266;
			this.scnView.TabIndex = 1;
			this.scnView.Text = "splitContainer1";
			// 
			// trvStructureView
			// 
			this.trvStructureView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trvStructureView.Location = new System.Drawing.Point(0, 0);
			this.trvStructureView.Name = "trvStructureView";
			this.trvStructureView.Size = new System.Drawing.Size(266, 426);
			this.trvStructureView.TabIndex = 0;
			// 
			// tlpDataView
			// 
			this.tlpDataView.ColumnCount = 2;
			this.tlpDataView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tlpDataView.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tlpDataView.Controls.Add(this.lblKey, 0, 0);
			this.tlpDataView.Controls.Add(this.lblKeyValue, 1, 0);
			this.tlpDataView.Controls.Add(this.lblValue, 0, 1);
			this.tlpDataView.Controls.Add(this.lblValueValue, 1, 1);
			this.tlpDataView.Controls.Add(this.lblType, 0, 2);
			this.tlpDataView.Controls.Add(this.lblTypeValue, 1, 2);
			this.tlpDataView.Controls.Add(this.lblVersion, 0, 3);
			this.tlpDataView.Controls.Add(this.lblVersionValue, 1, 3);
			this.tlpDataView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpDataView.Location = new System.Drawing.Point(0, 0);
			this.tlpDataView.Name = "tlpDataView";
			this.tlpDataView.RowCount = 4;
			this.tlpDataView.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tlpDataView.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tlpDataView.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tlpDataView.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpDataView.Size = new System.Drawing.Size(530, 426);
			this.tlpDataView.TabIndex = 0;
			// 
			// lblKey
			// 
			this.lblKey.AutoSize = true;
			this.lblKey.Location = new System.Drawing.Point(3, 0);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(29, 15);
			this.lblKey.TabIndex = 0;
			this.lblKey.Text = "Key:";
			// 
			// lblKeyValue
			// 
			this.lblKeyValue.AutoSize = true;
			this.lblKeyValue.Location = new System.Drawing.Point(57, 0);
			this.lblKeyValue.Name = "lblKeyValue";
			this.lblKeyValue.Size = new System.Drawing.Size(36, 15);
			this.lblKeyValue.TabIndex = 1;
			this.lblKeyValue.Text = "NULL";
			// 
			// lblValue
			// 
			this.lblValue.AutoSize = true;
			this.lblValue.Location = new System.Drawing.Point(3, 15);
			this.lblValue.Name = "lblValue";
			this.lblValue.Size = new System.Drawing.Size(38, 15);
			this.lblValue.TabIndex = 2;
			this.lblValue.Text = "Value:";
			// 
			// lblValueValue
			// 
			this.lblValueValue.AutoSize = true;
			this.lblValueValue.Location = new System.Drawing.Point(57, 15);
			this.lblValueValue.Name = "lblValueValue";
			this.lblValueValue.Size = new System.Drawing.Size(36, 15);
			this.lblValueValue.TabIndex = 3;
			this.lblValueValue.Text = "NULL";
			// 
			// lblType
			// 
			this.lblType.AutoSize = true;
			this.lblType.Location = new System.Drawing.Point(3, 30);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(34, 15);
			this.lblType.TabIndex = 4;
			this.lblType.Text = "Type:";
			// 
			// lblTypeValue
			// 
			this.lblTypeValue.AutoSize = true;
			this.lblTypeValue.Location = new System.Drawing.Point(57, 30);
			this.lblTypeValue.Name = "lblTypeValue";
			this.lblTypeValue.Size = new System.Drawing.Size(36, 15);
			this.lblTypeValue.TabIndex = 5;
			this.lblTypeValue.Text = "NULL";
			// 
			// lblVersion
			// 
			this.lblVersion.AutoSize = true;
			this.lblVersion.Location = new System.Drawing.Point(3, 45);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(48, 15);
			this.lblVersion.TabIndex = 6;
			this.lblVersion.Text = "Version:";
			// 
			// lblVersionValue
			// 
			this.lblVersionValue.AutoSize = true;
			this.lblVersionValue.Location = new System.Drawing.Point(57, 45);
			this.lblVersionValue.Name = "lblVersionValue";
			this.lblVersionValue.Size = new System.Drawing.Size(36, 15);
			this.lblVersionValue.TabIndex = 7;
			this.lblVersionValue.Text = "NULL";
			// 
			// FormViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.scnView);
			this.Controls.Add(this.mnsMenu);
			this.MainMenuStrip = this.mnsMenu;
			this.MinimumSize = new System.Drawing.Size(480, 360);
			this.Name = "FormViewer";
			this.Text = "BDS Viewer";
			this.mnsMenu.ResumeLayout(false);
			this.mnsMenu.PerformLayout();
			this.scnView.Panel1.ResumeLayout(false);
			this.scnView.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.scnView)).EndInit();
			this.scnView.ResumeLayout(false);
			this.tlpDataView.ResumeLayout(false);
			this.tlpDataView.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip mnsMenu;
		private System.Windows.Forms.SplitContainer scnView;
		private System.Windows.Forms.TreeView trvStructureView;
		private System.Windows.Forms.TableLayoutPanel tlpDataView;
		private System.Windows.Forms.ToolStripMenuItem tsmFile;
		private System.Windows.Forms.ToolStripMenuItem tsmFileOpen;
		private System.Windows.Forms.ToolStripSeparator sepFile;
		private System.Windows.Forms.ToolStripMenuItem tsmFileExit;
		private System.Windows.Forms.ToolStripMenuItem tsmAbout;
		private System.Windows.Forms.Label lblKey;
		private System.Windows.Forms.Label lblKeyValue;
		private System.Windows.Forms.Label lblValue;
		private System.Windows.Forms.Label lblValueValue;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.Label lblTypeValue;
		private System.Windows.Forms.Label lblVersion;
		private System.Windows.Forms.Label lblVersionValue;
	}
}

