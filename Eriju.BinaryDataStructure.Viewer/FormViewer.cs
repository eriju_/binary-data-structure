﻿using Eriju.BinaryDataStructure.Lib;
using Eriju.BinaryDataStructure.Lib.Elements;
using Eriju.BinaryDataStructure.Lib.Structure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Eriju.BinaryDataStructure.Viewer {
	public partial class FormViewer : Form {
		public FormViewer() {
			InitializeComponent();

			Text += $" [BDS Version {BDS.VERSION}]";

			tsmFileOpen.Click += TsmFileOpen_Click;
			trvStructureView.AfterSelect += TrvStructureView_AfterSelect;
			tsmFileExit.Click += TsmFileExit_Click;
			tsmAbout.Click += TsmAbout_Click;
		}

		private void TsmAbout_Click(object sender, EventArgs e) {
			MessageBox.Show(this, $"Binary Data Structure Viewer [Version {Program.VERSION}]\n© Erik Kilian Jung, 2020", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void TsmFileExit_Click(object sender, EventArgs e) {
			Application.Exit();
		}

		private void TrvStructureView_AfterSelect(object sender, TreeViewEventArgs e) {
			PopulateInfoView(e.Node);
		}

		private void TsmFileOpen_Click(object sender, EventArgs e) {
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Title = "Open BDS file";

			if (openFileDialog.ShowDialog(this) == DialogResult.OK) {
				using (BinaryReader binaryReader = new BinaryReader(File.Open(openFileDialog.FileName, FileMode.Open))) {
					PopulateStructureView(BDS.Read(binaryReader, out int version), trvStructureView);
					lblVersionValue.Text = version.ToString();
				}
			}
		}

		private void PopulateStructureView(BDSCompound bds, TreeView treeView) {
			treeView.Nodes.Clear();

			lblKeyValue.Text = "NULL";
			lblTypeValue.Text = "NULL";
			lblValueValue.Text = "NULL";

			TreeNode rootNode = new TreeNode(bds.Key);
			rootNode = GetTreeNodeFromBDS(bds, rootNode);
			treeView.Nodes.Add(rootNode);
		}

		private TreeNode GetTreeNodeFromBDS(BDSBase bds, TreeNode treeNode) {
			treeNode.Text = bds.Key;

			if (bds.GetType() == BDSType.COMPOUND) {
				foreach (KeyValuePair<string, BDSBase> entry in ((BDSCompound)bds).Value) {
					TreeNode node = new TreeNode(entry.Value.Key);
					node = GetTreeNodeFromBDS(entry.Value, node);
					node.Tag = entry.Value;
					treeNode.Nodes.Add(node);
				}
			}

			treeNode.Tag = bds;

			return treeNode;
		}

		private void PopulateInfoView(TreeNode node) {
			BDSBase bds = (BDSBase)node.Tag;

			lblKeyValue.Text = bds.Key;
			lblTypeValue.Text = $"{BDSType.GetTypeName(bds.GetType())} ({bds.GetType()})";
			lblValueValue.Text = "NULL";

			switch (bds.GetType()) {
				case BDSType.BOOL:
					lblValueValue.Text = ((BDSBool)bds).Value.ToString();
					break;

				case BDSType.BYTE:
					lblValueValue.Text = ((BDSByte)bds).Value.ToString();
					break;

				case BDSType.SBYTE:
					lblValueValue.Text = ((BDSSByte)bds).Value.ToString();
					break;

				case BDSType.CHAR:
					lblValueValue.Text = ((BDSChar)bds).Value.ToString();
					break;

				case BDSType.DECIMAL:
					lblValueValue.Text = ((BDSDecimal)bds).Value.ToString();
					break;

				case BDSType.DOUBLE:
					lblValueValue.Text = ((BDSDouble)bds).Value.ToString();
					break;

				case BDSType.FLOAT:
					lblValueValue.Text = ((BDSFloat)bds).Value.ToString();
					break;

				case BDSType.INT:
					lblValueValue.Text = ((BDSInt)bds).Value.ToString();
					break;

				case BDSType.UINT:
					lblValueValue.Text = ((BDSUInt)bds).Value.ToString();
					break;

				case BDSType.LONG:
					lblValueValue.Text = ((BDSLong)bds).Value.ToString();
					break;

				case BDSType.ULONG:
					lblValueValue.Text = ((BDSULong)bds).Value.ToString();
					break;

				case BDSType.SHORT:
					lblValueValue.Text = ((BDSShort)bds).Value.ToString();
					break;

				case BDSType.USHORT:
					lblValueValue.Text = ((BDSUShort)bds).Value.ToString();
					break;

				case BDSType.STRING:
					lblValueValue.Text = ((BDSString)bds).Value;
					break;

				case BDSType.BYTEARRAY:
					lblValueValue.Text = ((BDSByteArray)bds).Value.ToString();
					break;

				case BDSType.COMPOUND:
					lblValueValue.Text = "{ Compound }";
					break;
			}
		}
	}
}
