using System;
using System.Windows.Forms;

namespace Eriju.BinaryDataStructure.Viewer {
	static class Program {
		public const int VERSION = 1;

		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.SetHighDpiMode(HighDpiMode.SystemAware);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new FormViewer());
		}
	}
}
